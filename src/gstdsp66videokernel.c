/*
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <string.h>
#include "gstdsp66videokernel.h"

static GstStaticPadTemplate dsp66_video_kernel_src_factory =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("{ I420, YV12 }"))
    );

static GstStaticPadTemplate dsp66_video_kernel_sink_factory =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("{ I420, YV12 }"))
    );

/* Kernel signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

#define DEFAULT_FILTERSIZE   5
#define DEFAULT_LUM_ONLY     TRUE
#define DEFAULT_KERNELTYPE   GST_DSP66_VIDEO_KERNELTYPE_MEDIAN
#define DEFAULT_ARBKERNEL    "Sobel3x3"
enum
{
  PROP_0,
  PROP_FILTERSIZE,
  PROP_LUM_ONLY,
  PROP_KERNELTYPE,
  PROP_ARBKERNEL
};

#define GST_TYPE_VIDEO_KERNEL_FILTERSIZE (gst_dsp66_video_kernel_filtersize_get_type())
#define GST_TYPE_VIDEO_KERNELTYPE (gst_dsp66_video_kerneltype_get_type())

static const GEnumValue dsp66_video_kernel_filtersizes[] = {
  {GST_DSP66_VIDEO_KERNEL_FILTERSIZE_5, "Kernel of 5 neighbour pixels", "5"},
  {GST_DSP66_VIDEO_KERNEL_FILTERSIZE_9, "Kernel of 9 neighbour pixels", "9"},
  {GST_DSP66_VIDEO_KERNEL_FILTERSIZE_25, "Kernel of 25 neighbour pixels", "25"},
  {0, NULL, NULL},
};
static const GEnumValue dsp66_video_kerneltype[] = {
  {GST_DSP66_VIDEO_KERNELTYPE_MEDIAN, "Kernel median", "0"},
  {GST_DSP66_VIDEO_KERNELTYPE_SOBEL, "Kernel sobel", "1"},
  {GST_DSP66_VIDEO_KERNELTYPE_CONV,  "Kernel conv",  "2"},
  {GST_DSP66_VIDEO_KERNELTYPE_CANNY, "Kernel canny", "3"},
  {GST_DSP66_VIDEO_KERNELTYPE_ARB, "Kernel arbitrary", "4"},
  {0, NULL, NULL},
};

static GType
gst_dsp66_video_kernel_filtersize_get_type (void)
{
  static GType dsp66_video_kernel_filtersize_type = 0;

  if (!dsp66_video_kernel_filtersize_type) {
    dsp66_video_kernel_filtersize_type = g_enum_register_static ("GstDsp66VideoKernelFilterSize",
        dsp66_video_kernel_filtersizes);
  }
  return dsp66_video_kernel_filtersize_type;
}

static GType
gst_dsp66_video_kerneltype_get_type (void)
{
  static GType dsp66_video_kerneltype_type = 0;

  if (!dsp66_video_kerneltype_type) {
    dsp66_video_kerneltype_type = g_enum_register_static ("GstDsp66VideoKernelType", dsp66_video_kerneltype);
  }
  return dsp66_video_kerneltype_type;
}

#define gst_dsp66_video_kernel_parent_class parent_class
G_DEFINE_TYPE (GstDsp66VideoKernel, gst_dsp66_video_kernel, GST_TYPE_VIDEO_FILTER);

static GstFlowReturn gst_dsp66_video_kernel_transform_frame (GstVideoFilter * filter,
    GstVideoFrame * in_frame, GstVideoFrame * out_frame);

static void gst_dsp66_video_kernel_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_dsp66_video_kernel_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static void
gst_dsp66_video_kernel_class_init (GstDsp66VideoKernelClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;
  GstVideoFilterClass *vfilter_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;
  vfilter_class = (GstVideoFilterClass *) klass;

  gobject_class->set_property = gst_dsp66_video_kernel_set_property;
  gobject_class->get_property = gst_dsp66_video_kernel_get_property;

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_FILTERSIZE,
      g_param_spec_enum ("filtersize", "Filtersize", "The size of the filter (5,9,25)",
          GST_TYPE_VIDEO_KERNEL_FILTERSIZE, DEFAULT_FILTERSIZE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_KERNELTYPE,
      g_param_spec_enum ("kerneltype", "Kerneltype", "Type of kernel (0=median, 1=sobel 2=conv 3=arbitrary)",
          GST_TYPE_VIDEO_KERNELTYPE, DEFAULT_KERNELTYPE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_LUM_ONLY,
      g_param_spec_boolean ("lum-only", "Lum Only", "Only apply filter on "
          "luminance", DEFAULT_LUM_ONLY,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_ARBKERNEL,
      g_param_spec_string ("arbkernel", "User defined kernel",
          "The name of the kernel invoked via OpenCL.",
          NULL, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&dsp66_video_kernel_sink_factory));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&dsp66_video_kernel_src_factory));
  gst_element_class_set_static_metadata (gstelement_class, "Video kernels (choose: kerneltype, filtersize, lum-only, kernelname (opt))",
      "Video Kernel",
      "Apply a kernel via DSP C66 offload to an image",
      "based on work by: Wim Taymans <wim.taymans@gmail.com>");

  vfilter_class->transform_frame =
      GST_DEBUG_FUNCPTR (gst_dsp66_video_kernel_transform_frame);
}

void
gst_dsp66_video_kernel_init (GstDsp66VideoKernel * kernel)
{
  kernel->filtersize = DEFAULT_FILTERSIZE;
  kernel->kerneltype = DEFAULT_KERNELTYPE;
  kernel->lum_only   = DEFAULT_LUM_ONLY;
  kernel->arbkernel  = g_strdup (DEFAULT_ARBKERNEL);
}

extern int oclconv_kernel(int kernel_type, int kernel_size, char *arbkernel, 
                          unsigned char *src, unsigned char *dest, int width, int height, int sstride, int dstride); 

static int 
imgproc (gint kerneltype, gint filtersize, gchar *arbkernel, 
    guint8 * dest, gint dstride, 
    const guint8 * src, gint sstride,
    gint width, gint height)
{
  return oclconv_kernel(kerneltype, filtersize, arbkernel, (unsigned char *)src, (unsigned char *)dest, width, height, sstride, dstride);
}

static GstFlowReturn
gst_dsp66_video_kernel_transform_frame (GstVideoFilter * filter,
    GstVideoFrame * in_frame, GstVideoFrame * out_frame)
{
  GstDsp66VideoKernel *kernel = GST_DSP66_VIDEO_KERNEL (filter);

    if(imgproc (kernel->kerneltype, kernel->filtersize, kernel->arbkernel,
        GST_VIDEO_FRAME_PLANE_DATA (out_frame, 0),
        GST_VIDEO_FRAME_PLANE_STRIDE (out_frame, 0),
        GST_VIDEO_FRAME_PLANE_DATA (in_frame, 0),
        GST_VIDEO_FRAME_PLANE_STRIDE (in_frame, 0),
        GST_VIDEO_FRAME_WIDTH (in_frame), GST_VIDEO_FRAME_HEIGHT (in_frame)) < 0)
    {
      gst_video_frame_copy_plane (out_frame, in_frame, 0);
      gst_video_frame_copy_plane (out_frame, in_frame, 1);
      gst_video_frame_copy_plane (out_frame, in_frame, 2);
    }
    if (kernel->lum_only) {
      gst_video_frame_copy_plane (out_frame, in_frame, 1);
      gst_video_frame_copy_plane (out_frame, in_frame, 2);
    } else {
      imgproc (kernel->kerneltype, kernel->filtersize, kernel->arbkernel,
          GST_VIDEO_FRAME_PLANE_DATA (out_frame, 1),
          GST_VIDEO_FRAME_PLANE_STRIDE (out_frame, 1),
          GST_VIDEO_FRAME_PLANE_DATA (in_frame, 1),
          GST_VIDEO_FRAME_PLANE_STRIDE (in_frame, 1),
          GST_VIDEO_FRAME_WIDTH (in_frame) / 2,
          GST_VIDEO_FRAME_HEIGHT (in_frame) / 2);
      imgproc (kernel->kerneltype, kernel->filtersize, kernel->arbkernel,
          GST_VIDEO_FRAME_PLANE_DATA (out_frame, 2),
          GST_VIDEO_FRAME_PLANE_STRIDE (out_frame, 2),
          GST_VIDEO_FRAME_PLANE_DATA (in_frame, 2),
          GST_VIDEO_FRAME_PLANE_STRIDE (in_frame, 2),
          GST_VIDEO_FRAME_WIDTH (in_frame) / 2,
          GST_VIDEO_FRAME_HEIGHT (in_frame) / 2);
    }

  return GST_FLOW_OK;
}

static void
gst_dsp66_video_kernel_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstDsp66VideoKernel *kernel;

  kernel = GST_DSP66_VIDEO_KERNEL (object);

  switch (prop_id) {
    case PROP_FILTERSIZE:
      kernel->filtersize = g_value_get_enum (value);
      break;
    case PROP_LUM_ONLY:
      kernel->lum_only   = g_value_get_boolean (value);
      break;
    case PROP_KERNELTYPE:
      kernel->kerneltype = g_value_get_enum (value);
      break;
    case PROP_ARBKERNEL:
      if (!g_value_get_string (value)) {
        GST_WARNING ("Arbitrary kernel string can not be NULL");
        break;
      }
      g_free (kernel->arbkernel);
      kernel->arbkernel = g_value_dup_string (value);
      break;
    default:
      break;
  }
}

static void
gst_dsp66_video_kernel_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  GstDsp66VideoKernel *kernel;

  kernel = GST_DSP66_VIDEO_KERNEL (object);

  switch (prop_id) {
    case PROP_FILTERSIZE:
      g_value_set_enum (value, kernel->filtersize);
      break;
    case PROP_LUM_ONLY:
      g_value_set_boolean (value, kernel->lum_only);
      break;
    case PROP_KERNELTYPE:
      g_value_set_enum (value, kernel->kerneltype);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}
/** nothing past this point **/
