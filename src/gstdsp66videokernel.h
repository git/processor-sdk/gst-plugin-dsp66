/*
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __GST_DSP66_VIDEO_KERNEL_H__
#define __GST_DSP66_VIDEO_KERNEL_H__


#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>

G_BEGIN_DECLS

#define GST_TYPE_DSP66_VIDEO_KERNEL \
  (gst_dsp66_video_kernel_get_type())
#define GST_DSP66_VIDEO_KERNEL(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_DSP66_VIDEO_KERNEL,GstDsp66VideoKernel))
#define GST_DSP66_VIDEO_KERNEL_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_DSP66_VIDEO_KERNEL,GstDsp66VideoKernelClass))
#define GST_IS_DSP66_VIDEO_KERNEL(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_DSP66_VIDEO_KERNEL))
#define GST_IS_DSP66_VIDEO_KERNEL_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_DSP66_VIDEO_KERNEL))

typedef struct _GstDsp66VideoKernel GstDsp66VideoKernel;
typedef struct _GstDsp66VideoKernelClass GstDsp66VideoKernelClass;

typedef enum
{
  GST_DSP66_VIDEO_KERNEL_FILTERSIZE_5  = 5,
  GST_DSP66_VIDEO_KERNEL_FILTERSIZE_9  = 9,
  GST_DSP66_VIDEO_KERNEL_FILTERSIZE_25 = 25
} GstDsp66VideoKernelFilterSize;

typedef enum {
  GST_DSP66_VIDEO_KERNELTYPE_MEDIAN = 0,
  GST_DSP66_VIDEO_KERNELTYPE_SOBEL  = 1,
  GST_DSP66_VIDEO_KERNELTYPE_CONV   = 2,
  GST_DSP66_VIDEO_KERNELTYPE_CANNY  = 3,
  GST_DSP66_VIDEO_KERNELTYPE_ARB    = 4
} GstDsp66VideoKernelType;

struct _GstDsp66VideoKernel {
  GstVideoFilter parent;
  GstDsp66VideoKernelFilterSize filtersize;
  GstDsp66VideoKernelType kerneltype;
  gboolean lum_only;
  gchar *arbkernel;
};

struct _GstDsp66VideoKernelClass {
  GstVideoFilterClass parent_class;
};

GType gst_dsp66_video_kernel_get_type (void);

G_END_DECLS

#endif /* __GST_DSP66_VIDEO_KERNEL_H__ */
