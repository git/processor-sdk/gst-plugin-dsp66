/*
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#define PIX_SORT(a,b) { if ((a)>(b)) PIX_SWAP((a),(b)); }
#define PIX_SWAP(a,b) { unsigned char temp=(a);(a)=(b);(b)=temp; }
#define ARGS global uchar  *pInput,    global ushort *pBufGradX, \
             global ushort *pBufGradY, global ushort *pBufMag,   \
             global uchar  *pBufOut,   global uchar  *pScratch,  \
             global uint   *numItems,  ushort width, ushort height

/* imglib library functions */
void IMG_median_3x3_8 (const unsigned char *restrict in_data, int cols, unsigned char * restrict out_data);
void IMG_sobel_3x3_8  (const unsigned char *restrict in_data, unsigned char *restrict out_data, int rows, int cols);
void IMG_conv_3x3_i8_c8s (const unsigned char *restrict in_data, unsigned char *restrict out_data, int cols, const char *restrict mask, int shift);
void VLIB_Canny_Edge_Detection (ARGS);
kernel void canny_tiocl(ARGS)
{
    VLIB_Canny_Edge_Detection(pInput, pBufGradX, pBufGradY, pBufMag, pBufOut, pScratch, numItems, width, height);
}

kernel void Median3x3(global const uchar* src, global uchar *dest,
                      const int width, const int height,
                      const int dstride, const int sstride)
{
  int i;
  const int max_X = width - 1;
  const int max_Y = height - 1;
    for (i = 0; i < max_Y; i++) {
      IMG_median_3x3_8 ((const unsigned char *)src, max_X, (const unsigned char *)dest);
      src  += sstride;
      dest += dstride; 
    }
}

kernel void Sobel3x3(global const uchar* src, global uchar *dest,
                      const int width, const int height,
                      const int dstride, const int sstride)
{
  IMG_sobel_3x3_8((const unsigned char *)src, (const unsigned char *)dest, width, height);
}

kernel void Conv5x5(global const uchar* src, global uchar *dest,
                      const int width, const int height,
                      const int dstride, const int sstride)
{
  int i;
  const char conv_mask5x5[25] = {
     1, 4, 6, 4, 1,
     4,16,24,16, 4,
     6,24,36,24, 6,
     4,16,24,16, 4,
     1, 4, 6, 4, 1
  };
    for (i = 0; i < height; i++) {
      IMG_conv_5x5_i8_c8s ((const unsigned char *)src, (unsigned char *)dest, width, sstride, conv_mask5x5, 8); 
      src  += sstride;
      dest += dstride; 
    }
}

kernel void Median2x2 (global const uchar* src, global uchar *dest,
                      const int width, const int height,
                      const int dstride, const int sstride)
{
/***
    int id = get_global_id(0);
    c[id] = a[id] + b[id];
***/
  unsigned char p[5];
  int i, j, k;

    /* copy the top and bottom rows into the result array */
    for (i = 0; i < width; i++) {
      dest[i] = src[i];
      dest[(height - 1) * dstride + i] = src[(height - 1) * sstride + i];
    }

  /* process the interior pixels */
  for (k = 2; k < height; k++) {
    dest += dstride;
    src += sstride;

    dest[0] = src[0];
    for (j = 2, i = 1; j < width; j++, i++) {
      p[0] = src[i - sstride];
      p[1] = src[i - 1];
      p[2] = src[i];
      p[3] = src[i + 1];
      p[4] = src[i + sstride];
      PIX_SORT (p[0], p[1]);
      PIX_SORT (p[3], p[4]);
      PIX_SORT (p[0], p[3]);
      PIX_SORT (p[1], p[4]);
      PIX_SORT (p[1], p[2]);
      PIX_SORT (p[2], p[3]);
      PIX_SORT (p[1], p[2]);
      /* debug line */
      if(k > 24 && k < 32) dest[i] = 0;
      else
      dest[i] = p[2];
    }
    dest[i] = src[i];
  }
}
/* nothing past this point */
